'use strict'

import Olm from 'olm'
import * as sdk from "matrix-js-sdk";
import { LocalStorageCryptoStore } from 'matrix-js-sdk/lib/crypto/store/localStorage-crypto-store.js'
import fs from 'fs'
import { LocalStorage } from 'node-localstorage'

const CONFIG_FILE = "./config.json"

class Config {
    constructor() { 
        this.fileName = null
        this.user = {
            "cached": {
                "device_id": "", // device ID, 10 uppercase letters
                "access_token": ""  // cryptogr. access token
            },
            "credentials": {
                "id": "",
                "password": "",
                "device_name": "ReMythJS",
                "homeserver": "",
                "store": "store",
            },
            "e2e": false
        }
    }

    /** Read a configuration file and return a Config object
     * @param {*} file: string
     * @return {*} Config
     * @memberof Config
     */
    static fromFile(file) {
        let config = new Config()
        config.fileName = file // Save the file name to config so the toFile method can work without file name
        config.user = {...config.user, ...JSON.parse(fs.readFileSync(file, 'utf8'))}
        return config
    }

    toFile(file = null) {
        if (file == null) {
            if (this.fileName == null) {
                throw new Error("No file name specified")
            }
            file = this.fileName
        }
        fs.writeFileSync(file, JSON.stringify(this.user, null, 4), 'utf8')
    }
}

class Bot {
    constructor(config) {
        // Initialize some values
        this.config = config
        this.user_config = config.user
        this.cached = this.user_config.cached
        this.credentials = this.user_config.credentials
        this.e2e = this.user_config.e2e
        this.localStorage = new LocalStorage(this.credentials.store);
        // Initialize the Matrix client
        this._client = sdk.createClient({
            baseUrl: this.credentials.homeserver,
            accessToken: this.cached.access_token,
            userId: this.credentials.id,
            sessionStore: new sdk.WebStorageSessionStore(this.localStorage),
            cryptoStore: new LocalStorageCryptoStore(this.localStorage),
            deviceId: this.cached.device_id // E2E
        })
        if (this.e2e) {
            console.warn("End-to-end encryption is enabled, but this feature is not yet properly implemented")
            Olm.init().then(() => {
                console.log("Olm library initialized")
                try {
                    this._client.initCrypto()
                    console.log("Client crypto initialized")
                } catch (e) {
                    console.error("Client crypto initialize failed.")
                    console.error(e)
                }
            })
        }
        if (this.cached.access_token === "") {
            console.log("No cached access token found, logging in")
            this._client.login("m.login.password", {"user": this.credentials.id, "password": this.credentials.password}).then((response) =>{
                this.cached.access_token = response.access_token
                this.cached.device_id = response.device_id // E2E
                this.config.toFile() // Save the access token to the config file.
                console.log("Saved access token and device id.")
            })
        }
    }
    async start() {
        this._client.startClient()
        this._client.once('sync', function(state) {
            if (state !== "PREPARED") {
                throw new Error("Failed to sync the client.")
            }
        });
        // TODO: Add a listener for the 'room.message' event
    }
}

async function main() {
    try {
        const cfg = Config.fromFile(CONFIG_FILE)
        // console.log(config)
        // Configuration without sensitive information so we can print that out.
        const cfgSafe = structuredClone(cfg) // Great stuff from NodeJS here, no more JSON trick.
        delete cfgSafe.user.cached
        delete cfgSafe.user.credentials
        console.log("Configuration file:", cfgSafe)
        const bot = new Bot(cfg)
        await bot.start()
        // TODO
    } catch (e) {
        if (e.code === 'ENOENT') {
            console.log('Config file not found, creating...')
            const cfg = new Config()
            try {
                cfg.toFile(CONFIG_FILE)
                console.log("New config file created, please edit it and run again.")
            } catch (e) {
                console.error(`Error occurred while creating a new file: \n${e.message}`)
            }
        }
        console.log('An error occurred while the bot is running:\n' + e)
        process.exit(1)
    }
}
await main()